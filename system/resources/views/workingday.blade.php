@extends("layaout")

@section('titulo', 'Solicitudes')




@section('Content')
<a href="{{ route('Home') }}">Home</a>
    <a href="{{ route('People') }}">Beneficiarios</a>
    <a href="{{ route('solicitudes') }}">Repartos</a>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h1 class="card-title text-center text-danger font-italic">Repartos</h1>
                </div>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-create-solicitud">
                    Nueva entrega
                </button>


            <!-- /.card-header -->
            <div class="card-body">
                <table id="posts" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Ticket</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Fecha</th>
                            <th>Acciones</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($p as $i)
                        <tr>
                            <td>{{ $i->id }}</td>
                            <td>{{$i->Ticket}}</td>
                            <td>{{$i->Name}}</td>
                            <td>{{$i->Lastname}}</td>
                            <td>{{ $i->date }}</td>
                            <td>
                                <button type="button" class="btn btn-warning mb-2 w-50" data-toggle="modal" data-target="#modal-update-{{$i->id}}">
                                    Editar
                                </button>
                                <form action="{{route('Delete', $i->id)}}" method="POST">
                                    {{ csrf_field() }}
                                    @method('DELETE')
                                    <button class="btn btn-danger w-50">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                        <div class="modal fade" id="modal-update-{{$i->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content bg-default">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Actualizar Solictud</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                    </div>
                                    <form action="{{ route('update',  $i->id) }}" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-body">

                                            <div class="form-group">
                                                <label for="people-id">Ticket</label>
                                                <select name="people_id" id="people-id" class="form-control">
                                                    <option value="{{$i->id}}">{{$i->Ticket}}::{{$i->Name}}</option>
                                                    @foreach ($persons as $x)
                                                    <option value="{{$x->id}}">{{$x->Ticket}}::{{$x->Name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="date">Fecha</label>
                                                <input id="date" type="date" name="date" class="form-control"  value="{{$date}}">
                                            </div>

                                        </div>

                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-outline-primary">Guardar</button>
                                        </div>
                                    </form>
                                </div>
                            <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>

                        <!-- /.modal -->

                        @endforeach


                        <div class="modal fade" id="modal-create-solicitud" >
                            <div class="modal-dialog bg-primary">
                                <div class="modal-content bg-default">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Crear solictud</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                    </div>
                                    <form action="{{ route('Crear') }}" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-body">

                                            <div class="form-group">
                                                <label for="people-id">Ticket</label>
                                                <select name="people_id" id="people-id" class="form-control">
                                                    <option value="">Selecione</option>
                                                    @foreach ($persons as $i)
                                                        <option value="{{$i->id}}">{{$i->Ticket}}::{{$i->Name}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                            <div class="form-group">
                                                <label for="">Fecha</label>
                                                <input type="date" name="date" value="{{$date}}"  class="form-control">
                                            </div>

                                        </div>

                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-outline-primary">Guardar</button>
                                        </div>
                                    </form>
                                </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>




@endsection

