
@extends("layaout");


@section('titulo', 'Beneficiarios')

@section("Content")


<a href="{{ route('Home') }}">Home</a>
<a href="{{ route('People') }}">Beneficiarios</a>
<a href="{{ route('solicitudes') }}">Repartos</a>
    <div style="width: 100%;">





        <div class="row">


            <div class="col-lg-12">
                <h1 class="text-danger  text-center">Beneficiarios</h1>
                <button type="button" class="btn btn-primary btn-lg m-3" data-toggle="modal" data-target="#modal-create-beneficiarios">
                    Nuevo
                </button>

                <table class="table table-bordered  ">
                    <thead class="bg-dark">
                      <tr class="" >
                        <th class="text-white" scope="col">#</th>
                        <th class="text-white" scope="col">Nombre</th>
                        <th class="text-white" scope="col">Apellido</th>
                        <th class="text-white" scope="col">Cedula</th>
                        <th class="text-white" scope="col">Fecha de Nacimiento</th>
                        <th class="text-white" scope="col">Edad</th>

                        <th class="text-white" scope="col">Telefono</th>
                        <th class="text-white" style="text-align: center" scope="col">Correo</th>
                        <th class="text-white" scope="col">Dirección</th>


                        <th class="text-white" scope="col">Estado</th>
                        <th class="text-white" scope="col">Ticket</th>
                        <th class="text-white" scope="col">Acciones</th>




                    </tr>
                    </thead>
                    <tbody>
                        <?php
                             use Carbon\Carbon;



                            foreach ($p as $i) {
                         ?>
                         <tr  class="color-row">
                            <th scope="row">{{$i->id}}</th>
                            <td>{{$i->Name}}</td>
                            <td>{{$i->Lastname}}</td>
                            <td>{{$i->Identification}}</td>
                            <td>{{$i->dates}} </td>
                            <td>{{Carbon::createFromDate($i->dates)->age}}</td>
                            <td>{{$i->t}}</td>
                            <td>{{$i->email}} </td>
                            <td><button type="button" class="btn btn-primary  text-center">
                                Ver
                            </button></td>

                            <td>{{$i->status}} </td>
                            <td>{{$i->Ticket}} </td>
                            <td>

                               <div class="row">
                                <button type="button" class="btn btn-warning mb-2 w-50" data-toggle="modal" data-target="#modal-update-{{$i->id}}">
                                    Editar
                                </button>
                                <form action="{{route('Delete-people', $i->id)}}" method="POST">
                                    {{ csrf_field() }}
                                    @method('DELETE')
                                    <button class="btn btn-danger w-50">Eliminar</button>
                                </form>

                               </div>


                            </td>





                        </tr>
                        <div class="modal fade" id="modal-update-{{$i->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content bg-default">
                                    <div class="modal-header bg-dark">
                                        <h2 class="modal-title justify-content-center text-white">Actualizar Solicitud</h2>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                    </div>
                                    <form  action="{{ route('update-people',  $i->id) }}" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-body ">



                                            <div class="form-group">
                                                <label for="">Nombre</label>
                                                <input value="{{$i->Name}}" type="text" name="Name" placeholder="Nombre"  class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Apellido</label>
                                                <input value="{{$i->Lastname}}" type="text" name="Lastname" placeholder="Apellido"  class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Cedula</label>
                                                <input value="{{$i->Identification}}" type="text" name="Cedula" placeholder="Cedula"  class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Fecha de Nacimiento</label>
                                                <input value="{{$i->dates}}" type="date" name="date"   class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Telefono</label>
                                                <input value="{{$i->t}}" type="text" name="cell" placeholder="Telefono"  class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Correo</label>
                                                <input value="{{$i->email}}" type="text" name="Email" placeholder="Correo"  class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Direccion</label>
                                                <input type="text" value="{{$i->a}}" name="adress"  class="form-control">

                                            </div>

                                            <div class="form-group">
                                                <label for="">Ticket</label>
                                                <input value="{{$i->Ticket}}" type="text"  name="Ticket" class="form-control" placeholder="Ticket"  ></input>

                                            </div>
                                            <div class="form-group text-center ">
                                                <h3 class="text-center m-2 ">Estado</h3>

                                                <div class="form-check d-inline m-2">
                                                    <input class="form-check-input" type="radio" name="status" id="exampleRadios1" value="Retirado" checked>
                                                    <label class="form-check-label" for="exampleRadios1">
                                                      Retirado
                                                    </label>
                                                </div>
                                                <div class="form-check d-inline" active>
                                                    <input class="form-check-input" type="radio" name="status" id="exampleRadios1" value="Activo" checked>
                                                    <label class="form-check-label" for="exampleRadios1">
                                                      Activo
                                                    </label>
                                                </div>




                                            </div>

                                        </div>

                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-outline-primary">Guardar</button>
                                        </div>
                                    </form>
                                </div>
                            <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>

                        <?php
                            }

                        ?>



                    </tbody>
                  </table>


            </div>

        </div>





    </div>

    <div class="modal fade" id="modal-create-beneficiarios" >
        <div class="modal-dialog bg-primary">
            <div class="modal-content bg-default">
                <div class="modal-header">
                    <h4 class="modal-title">Crear solictud</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <form action="{{route('registrar-be')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="">Nombre</label>
                            <input  type="text" name="Name" placeholder="Nombre"  class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Apellido</label>
                            <input type="text" name="Lastname" placeholder="Apellido"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Cedula</label>
                            <input type="text" name="Cedula" placeholder="Cedula"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Fecha de Nacimiento</label>
                            <input type="date" name="date"   class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Telefono</label>
                            <input type="text" name="phone" placeholder="Telefono"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Correo</label>
                            <input type="text" name="Email" placeholder="Correo"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Direccion</label>
                            <textarea name="adress" class="form-control" placeholder="Direccion" rows="3"></textarea>

                        </div>
                        <div class="form-group">
                            <label for="">Ticket</label>
                            <input type="text"  name="Ticket" class="form-control" placeholder="Ticket" ></input>

                        </div>


                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline-primary">Guardar</button>
                        <button type="reset" class="btn btn-outline-danger">Cancelar</button>
                    </div>
                </form>
            </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>













@endsection




