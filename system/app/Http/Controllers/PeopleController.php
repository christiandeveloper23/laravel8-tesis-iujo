<?php

namespace App\Http\Controllers;
use App\Models\Phone;
use App\Models\person;
use Faker\Provider\bg_BG\PhoneNumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $p = DB::table('people')
        ->join('phones','phones.id' , '=' , 'people.phone_id')
        ->select('people.id as id',
        'people.name as Name',
        'people.Lastname as Lastname',
        'people.Cedula as Identification',
        'people.Dateofbirth as dates',
        'people.Email as email',
        'phones.Phone as t',
        'people.adress as a',
        'people.status as status',
        'people.Ticket as Ticket'
        )
        ->orderBy('id', 'desc')

        ->get();

        return View("People",['p'=>$p]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cell = new Phone();
        $cell->Phone = $request->phone;
        $cell->save();

        $id = $cell->id;

        $people = new person();
        $people->Name = $request->Name;
        $people->Lastname = $request->Lastname;
        $people->Cedula = $request->Cedula;
        $people->Dateofbirth = $request->date;
        $people->phone_id = $id;
        $people->Email = $request->Email;
        $people->adress = $request->adress;
        $people->Ticket = $request->Ticket;
        $people->save();

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $people = Person::find($id);
        $identification=$people->phone_id;

        $phones = Phone::find($identification);
        $phones->Phone = $request->cell;
        $phones->save();

        $people->Name = $request->Name;
        $people->Lastname = $request->Lastname;
        $people->Cedula = $request->Cedula;
        $people->Dateofbirth = $request->date;
        $people->Email = $request->Email;
        $people->adress = $request->adress;
        $people->Ticket = $request->Ticket;
        $people->status = $request->status;

        $people->save();

        return redirect()->back();




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $people = Person::find($id);
        $people->delete();
        return redirect()->back();
    }

}
