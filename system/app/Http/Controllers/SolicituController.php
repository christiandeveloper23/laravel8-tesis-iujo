<?php

namespace App\Http\Controllers;
use App\Models\workingday;
use App\Models\person;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class SolicituController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workings = workingday::all();
        $date= date("Y-m-d");
        $persons= person::all();
        $p = DB::table('workingdays')
        ->join('people','people.id' , '=' , 'workingdays.people_id')
        ->select('workingdays.id as id',
        'people.Ticket as Ticket',
        'people.Name as Name',
        'people.Lastname as Lastname',
        'workingdays.date as date'
        )
        ->orderBy('id', 'desc')

        ->get();

        return View("workingday",['p'=>$p,'persons'=>$persons,'date'=>$date,'workings'=>$workings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $working = new workingday();

        $working->people_id = $request->people_id;
        $working->date= $request->date;
        $working->save();

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $working = workingday::find($id);

        $working->people_id = $request->people_id;
        $working->date= $request->date;
        $working->save();

       return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $working = workingday::find($id);
        $working->delete();
        return redirect()->back();
    }
}
