<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class person extends Model
{

    public $timestamps = false;

    // protected $fillable = ['Name',
    // 'Lastname',
    // 'Identification card',
    // 'Date of birth',
    // 'Email',
    // 'phone_id',
    // 'status',
    // 'Ticket'
    // ];

    public function workingdays() {
        return $this->hasMany('App\Models\workingday');
    }


}
