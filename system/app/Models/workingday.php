<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class workingday extends Model
{
    public $timestamps = false;


    public function person() {
        return $this->belongsTo('App\Models\person');
    }
}

