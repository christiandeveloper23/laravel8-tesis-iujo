<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    public $timestamps = false;


    // protected $fillable=['id','Phone'];


    public function Person()
    {
        return $this->hasOne('App\Models\Person'::class);
    }
}
