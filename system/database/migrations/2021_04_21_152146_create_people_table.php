<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('id');
            $table->string('Name');
            $table->string('Lastname');
            $table->bigInteger('Cedula');
            $table->date('Dateofbirth');
            $table->text('adress')->nullable();
            $table->string('Email',100)->unique();
            $table->bigInteger('phone_id')->unsigned();
            $table->foreign("phone_id")->references('id')->on('phones')->onDelete('cascade')->onUpdate('cascade');
            $table->string('status')->default('Activo')->nullable();
            $table->string('Ticket',6)->unique()->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
