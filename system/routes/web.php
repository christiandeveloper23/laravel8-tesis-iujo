<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Http\Controllers\PeopleController;
use App\Http\Controllers\solicitu;
use App\Http\Controllers\SolicituController;

Route::view('/', 'Home')->name('Home');

Route::get('/Personas', [PeopleController::class, 'index'])->name('People');
Route::post('/Personas/create', [PeopleController::class, 'store'])->name("registrar-be");
Route::post('/Personas/{id}/update', [PeopleController::class, 'update'])->name("update-people");
Route::delete('/Personas/{id}/delete', [PeopleController::class, 'destroy'])->name("Delete-people");

Route::get('/Solicitudes', [SolicituController::class, 'index'])->name('solicitudes');
Route::post('/Solicitudes/Crear', [SolicituController::class, 'store'])->name('Crear');
Route::post('/Solicitudes/{id}/Actualizar', [SolicituController::class, 'update'])->name('update');
Route::delete('/Solicitudes/{id}/Eliminar', [SolicituController::class, 'destroy'])->name('Delete');



